#!/bin/bash

# Asks the user if they agree to launch the configuration
echo "The purpose of this script is to :"
echo "- Copy the inventory file 'inventory.ini' to the directory '/etc/ansible/host/'."
echo "- Add the DNS entries for the ansible machines to the '/etc/hosts' file"
read -p "Are you sure you want to run the Ansible configuration ? (y/n) : : " response

# Checks the user's response
if [ "$response" != "y" ]; then
  echo "Deny by user. Abort..."
  exit 0
fi

# Displays a message to the user
echo "Configuration in progress. Please wait..."

#PLAYBOOK_FILE="playbook.yml"
INVENTORY_FILE="inventory.ini"
HOSTS_FILE="hosts"
DESTINATION="/etc/ansible/hosts"
DEST_INVENTORY_FILE="/etc/ansible/host/inventory.ini"*


# Checks whether the inventory.ini file exist
if [ ! -f "$INVENTORY_FILE" ]; then
  echo "ERROR : INVENTORY FILE $INVENTORY_FILE DOESN'T EXIST."
  exit 1
fi

# Copy the inventory file to /etc/ansible/hosts with sudo rights
sudo mkdir -p "/etc/ansible/hosts"
sudo cp -p "$INVENTORY_FILE" "$DESTINATION"


# Checks whether the copy was successful
if [ $? -eq 0 ]; then
  echo "Copy the inventory file to $DESTINATION..."
  echo "The inventory file has been successfully copied to $DESTINATION."
# We add the entries in the "hosts" file to the "/etc/hosts" file
  echo "DNS entries added to the '/etc/hosts' file:"
  cat "$HOSTS_FILE" | sudo tee -a /etc/hosts
  echo ""
  echo "End of script. Thank you for your confidence."
else
  echo "Error when copying the inventory file. Abort..."
  exit 1
fi

