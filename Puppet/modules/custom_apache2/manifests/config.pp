class custom_apache2::config {
  file { '/etc/apache2/apache2.conf':
    ensure  => file,
    notify  => Service[$custom_apache2::params::service],
  }
}
