class custom_apache2::service {
  service {
    $custom_apache2::params::service:
    ensure => running,
    enable => true,
    require => Package[$custom_apache2::params::apache2_package],
  }
}
