class custom_apache2::params {
  case $facts['os']['family'] {
    'Debian': {
      $apache2_package = 'apache2'
      $service = 'apache2'
    }
    'RedHat': {
      $apache2_package = 'httpd'
      $service = 'httpd'
    }
    default: {
      fail("Unsupported operating system: ${::operatingsystem}")
    }
  }
}
