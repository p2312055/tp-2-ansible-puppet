class custom_apache2 {
  Class['custom_apache2::params']~>Class['custom_apache2::install']~>Class['custom_apache2::config']~>Class['custom_apache2::service']

  # Include the required dependencies
  include custom_apache2::params
  include custom_apache2::install
  include custom_apache2::config
  include custom_apache2::service
}
