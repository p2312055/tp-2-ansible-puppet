class custom_sshd::service {
  service {
    $custom_sshd::params::service:
      ensure => running,
      enable => true,
      require => Package[$custom_sshd::params::sshd_package],
  }
}
