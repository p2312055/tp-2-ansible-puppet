class custom_sshd::params {

  $sshd_package = $facts['os']['family'] ? {
    'Debian'  => 'openssh-server',
    'RedHat'  => 'openssh-server',
    default   => 'openssh-server',
  }

  $service = $facts['os']['family'] ? {
    'Debian'  => 'ssh',
    'RedHat'  => 'sshd',
    default   => 'sshd',
  }

  $sshd_config_path = $facts['os']['family'] ? {
    'Debian'  => '/etc/ssh/sshd_config',
    'RedHat'  => '/etc/ssh/sshd_config',
    default   => '/etc/ssh/sshd_config',
  }

}
