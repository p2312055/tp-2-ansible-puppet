class custom_sshd {
  Class['custom_sshd::params']~>Class['custom_sshd::install']~>Class['custom_sshd::config']~>Class['custom_sshd::service']

  # include dependencies
  include custom_sshd::params
  include custom_sshd::install
  include custom_sshd::config
  include custom_sshd::service
}
