class custom_sshd::install {
  package { $custom_sshd::params::sshd_package:
    ensure => installed,
  }
}
