class custom_sshd::config {
include custom_sshd::params  
file
 {
   '/etc/ssh/sshd_config':
    ensure  => file,
    content => template('custom_sshd/sshd_config.erb'),
    notify  => Service[$custom_sshd::params::service],
  }
}
