## Introduction 🐧

In this exercise, we're going to try out two configuration management and automation tools for administering IT systems, Ansible and puppet.


### Plan to be followed 🚩

- Introduction to configuration management
- Overview of configuration management tools
    - Definition
    - Automation
    - Orchestration
- Presentation of Ansible
    - Definition 
    - How it works
    - The components
- Presentation of ah-doc commands
- Presentation of playbooks
    - Variables, loops, jinja2 templates, filters
    - Notion of roles, tags, facts
- Presentation of ansible-galaxy



# Ansible 🐧

 For Ansible, we have to produce many Playbooks that will enable you to install an SSH server and the Apache2 service under ==**Debian**== or ==**Fedora**==. 
These Playbooks can be used on both systems (if ansible detect the task is only for a debian system, he will skip the task for other system (fedora for exemple)). 

## How Ansible Works ? 🤔

Ansible works as follows:

You need to run a script called "Ansible-Configuration.sh". Its purpose is to :
- Create a directory "*/etc/ansible/hosts/*" if it does **not exist**.
- Copy the inventory file into the previously created directory.
- Write the IP configuration and their machine name in the "*/etc/hosts*" file in relation to the "*hosts"* text file in the "*Ansible-conf*" directory.

Once this has been done, you then need to run the "**main-playbook.yml**" file, whose sole purpose is to run the three playbooks:
- "**playbook-SSH.yml**"
- "**playbook-VM.yml**"
- "**playbook-Apache2.yml**".

## PLaybooks 📚

The purpose of the "**playbook-VM.yml**" file is to : 
- Update the Debian VM
- Update the Fedora Server VM


The purpose of the "**playbook-SSH.yml**" file is to :
- **Install** and **activate** the `openssh-server` service on the Debian and Fedora Server VMs (although it must already be configured in order to be able to manage them via SSH)
- Change the SSH ports from **22** to **2222** on these two VMs
- Make sure that for the Fedora server VM, port **2222** is authorised on the firewall (with the **firewall daemon** (firewalld)) and on the security module (**SE-Linux**).
- **Restart** the SSH service on both VMs


The purpose of the "*playbook-Apache2.yml*" file is to : 
- **Install** and **activate** the *apache2* (for **Debian**) and *HTTPD* (for **Fedora Server**) servers, both of which are Apache Web services
- Make sure that for the Fedora Server VM, port **80** is authorised on the firewall (with the **firewall daemon** (firewalld)) and on the security module (SE-Linux).
- **Restart** the Web service on both VMs


  # Directory structure :  📁

```
  ┌─[etudyan@parrot]─[~/tp-2-ansible-puppet]
└──╼ $tree
.
├── Ansible
│   ├── Ansible-Conf
│   │   ├── Ansible-Configuration.sh
│   │   ├── hosts
│   │   └── inventory.ini
│   ├── ansible.cfg
│   └── playbook
│       ├── SSH
│       │   └── playbook-SSH.yml
│       ├── Update
│       │   └── playbook-VM.yml
│       ├── Web
│       │   └── playbook-Apache2.yml
│       └── main-playbook.yml
├── LICENSE
├── Puppet
│   ├── manifests
│   │   └── node.pp
│   └── modules
│       ├── custom_apache2
│       │   └── manifests
│       │       ├── config.pp
│       │       ├── init.pp
│       │       ├── install.pp
│       │       ├── params.pp
│       │       └── service.pp
│       └── custom_sshd
│           ├── manifests
│           │   ├── config.pp
│           │   ├── init.pp
│           │   ├── install.pp
│           │   ├── params.pp
│           │   └── service.pp
│           └── templates
│               └── sshd_config.erb
└── README.md

```

# The practice

## Ansible configuration

**WARNING** : You have to install the `sudo` package before execute theses commands !
To install `ansible` on your physical machine, you need to install the following ansible package and requirements:

- For **Debian** user, oldyyyyy : 🧐

```bash
sudo apt update
sudo apt install ansible git ssh-pass git -y
```

- For "**BTW I use Arch**" User : 🤓

```bash
sudo pacman -SY
sudo pacman -S ansible-core ansible ssh-pass git
```

- For **Open-Suse Leap** User, Because I love cameleons : 🦎

```bash
sudo zypper addrepo https://download.opensuse.org/repositories/systemsmanagement:/ansible:/release/openSUSE_Leap_15.3/systemsmanagement:ansible:release.repo
sudo zypper refresh
sudo zypper install ansible git
```
or for **openSUSE Tumbleweed** User : 🦎

```bash
sudo zypper addrepo https://download.opensuse.org/repositories/systemsmanagement:/ansible:/release/openSUSE_Tumbleweed/systemsmanagement:ansible:release.repo
sudo zypper refresh
sudo zypper install ansible git
```

- Finally, for **Rocky-Linux** User, the professional side : 😎

```bash
sudo dnf install epel-release -y
sudo dnf install ansible git -y
```

That's It ! :D

## Virtual Machine 💻

For Ansible playbooks to work, you first need to have theses VM:

- A Debian virtual machine (preferably 12 Bookworm), connected to the internet 
(**Link to the Debian ISO :** https://www.debian.org/distrib/ )

- A Fedora Server virtual machine (Version 40 preferred, DVD ISO version, **no QEMU** !), also connected to the Internet
(**Link to Fedora Server ISO :** https://fedoraproject.org/en/server/download )


## The basic configuration of these servers 📜
### Debian

To ensure that your debian machine works properly, you need to install the following packages with the correct configuration:

- Create the user etudyan :

```bash
adduser etudyan
passwd etudyan
```
Here, for simplicity, the username and password are the same

- Name the machine with the following name: "VM1", you can go to the "/etc/hostname" file to assign the name for the debian VM.

- Installation of the openssh-server package :
```bash
apt update
apt install openssh-server
```

Then activate this service :

```bash
systemctl start openssh-server && systemctl enable openssh-server
```

After that, you need to install the sudo package so that you can run the basic commands :

```bash
apt install sudo
```

Then configure the `/etc/sudoers` file with your best file-editing tool (nano or vim for example) to add the following configuration :

 ``` bash
# User privilege specification
root ALL=(ALL:ALL) ALL
etudyan ALL=(ALL:ALL) ALL
```

When this is done, get the IP address of your debian in order to add it in the "host" file of the ansible project "TP-2-ANSIBLE-PUPPET/Ansible/Ansible-Conf/hosts", replace the IP address obtained by the IP address of the file of the line corresponding to the "VM1" machine.

The configuration of your Debian 12 Bookworm machine is now **complete**.


### Fedora

For Fedora Server, you need to install the following packages with the correct configuration:

- Create the user etudyan :

```bash
adduser etudyan
passwd etudyan
```
Here, for simplicity, the username and password are the same

- Name the machine with the following name: "VM2", you can go to the "/etc/hostname" file to assign the name for the Fedora VM.

- Installation of the openssh-server package :

```bash
dnf install openssh-server
```

Then activate this service :

``` bash
systemctl start openssh-server && systemctl enable openssh-server
```

The sudo package is already installed on RedHat-based distributions. You'll need to configure the `/etc/sudoers` file with your best file-editing tool (nano or vim for example) to add the following configuration:

 ```bash
# User privilege specification
root ALL=(ALL:ALL) ALL
etudyan ALL=(ALL:ALL) ALL
```

When this is done, get the IP address of your Fedora in order to add it in the "host" file of the ansible project "TP-2-ANSIBLE-PUPPET/Ansible/Ansible-Conf/hosts", replace the IP address obtained by the IP address of the file of the line corresponding to the "VM2" machine.

The configuration of your Fedora Server 40 machine is now **complete**.


## Ansible configuration 📜

For ansible to work properly, you need to do the following:

- Go to the "TP-2-ANSIBLE-PUPPET/Ansible/Ansible-Conf/hosts" file and check that your virtual machines are listed there. 
For my current configuration :

```txt
192.168.1.102    VM1
192.168.1.164    VM2
```

As a reminder:

**VM1 ==> Debian**
**VM2 ==> Fedora-Server**

- When your configuration is correct, simply run the file `"Ansible-Configuration.sh"` :

```bash
┌─[etudyan@parrot]─[~/tp-2-ansible-puppet/Ansible/Ansible-Conf]
└──╼ $./Ansible-Configuration.sh 
The purpose of this script is to :
- Copy the inventory file 'inventory.ini' to the directory '/etc/ansible/host/'.
- Add the DNS entries for the ansible machines to the '/etc/hosts' file
Are you sure you want to run the Ansible configuration ? (y/n) : : y
Configuration in progress. Please wait...
Copy the inventory file to /etc/ansible/hosts...
The inventory file has been successfully copied to /etc/ansible/hosts.
DNS entries added to the '/etc/hosts' file:
192.168.1.102    VM1
192.168.1.164    VM2

End of script. Thank you for your confidence.
```
This script will copy the inventory file in the directory `"/etc/ansible/hosts/"` (default inventory directory for ansible) and copy the hosts file text in the hosts configuration file `"/etc/hosts"`.

Then we can check that our hosts file has been added to the configuration in `"/etc/hosts"`:

Contents of the `"/etc/hosts"` file:

```bash
┌─[etudyan@parrot]─[~/tp-2-ansible-puppet/Ansible/Ansible-Conf]
└──╼ $cat /etc/hosts
# Host addresses
127.0.0.1  localhost
127.0.1.1  parrot
::1        localhost ip6-localhost ip6-loopback
ff02::1    ip6-allnodes
ff02::2    ip6-allrouters
# Others
192.168.1.102    VM1
192.168.1.164    VM2
```

End of this part

## Launch Playbooks 📚

After theses configuration, we can see our `"inventory.ini"` file : 

```bash
[dev-old-port]
VM1 ansible_port=22 ansible_user=etudyan ansible_ssh_pass=etudyan ansible_become=yes ansible_become_method=sudo ansible_become_password=etudyan
VM2 ansible_port=22 ansible_user=etudyan ansible_ssh_pass=etudyan ansible_become=yes ansible_become_method=sudo ansible_become_password=etudyan


[dev-new-port]
VM1 ansible_port=2222 ansible_user=etudyan ansible_ssh_pass=etudyan ansible_become=yes ansible_become_method=sudo ansible_become_password=etudyan
VM2 ansible_port=2222 ansible_user=etudyan ansible_ssh_pass=etudyan ansible_become=yes ansible_become_method=sudo ansible_become_password=etudyan
```

I made the choice here to configure SSH and Apache 2 on both VMs. So in this inventory file, we can choose the group of machines and their ports according to the Ansible post-configuration (so when we still have an ssh port in 22), or when the configuration is done and we have to reach the machines with the new port 2222.

**Following a strange problem with Ansible, I was unable to resolve it without modifying the "inventory.ini" file.**

For the playbooks to work, it is necessary to comment out all the lines in the inventory containing the port "2222", otherwise, if we launch a group of machines from the inventory still having the port "*22*", Ansible will launch still in all cases, the ssh connection of the machines with port "*2222*"...

I also made the choice to run the playbook in a development environment with the root user, and in a production environment, with the etudyan user.

Quick explanation of an inventory line:
```
VM1 ansible_port=22 ansible_user=etudyan ansible_ssh_pass=etudyan ansible_become=yes ansible_become_method=sudo ansible_become_password=etudyan
```

- **VM1** : The name of the target host.
- **ansible_port=22** : The SSH port to use to connect to the host.
- **ansible_user=etudyan** : The username to use to connect to the host via SSH.
- **ansible_ssh_pass=etudyan** : The SSH password for the specified user (etudyan in this case).
- **ansible_become=yes** : Indicates that Ansible should use privilege escalation to execute commands on the host.
- **ansible_become_method=sudo** : Specifies the method to use for privilege escalation.
- **ansible_become_password=etudyan** : The password for privilege escalation.


So, to run our playbooks for the first time in our **development environment**, I suggest you do the following command:

```
┌─[etudyan@parrot]─[~/tp-2-ansible-puppet/Ansible]
└──╼ $ansible-playbook -l dev-old-port playbook/main-playbook.yml
```

If you are running the playbooks the second time, this time in a **production environment**, I recommend that you do the following:

```
┌─[etudyan@parrot]─[~/tp-2-ansible-puppet/Ansible]
└──╼ $ansible-playbook -l prod-new-port playbook/main-playbook.yml
```

End of Ansible tutorial !



# Puppet 

## Grading reminder:
• Git/GitLab 4 points (+1)
• Puppet Manifests 5 points (+1)
• Ansible Playbooks 5 points
• Puppet Environments 3 points
• Ansible Inventory 3 points

We propose to write Puppet manifests and Ansible playbooks to automatically configure the installation of SSH and Apache services, based on a number of system-specific parameters, whose values will be given by Facter.
• Tool Comparison 2 points Score out of 24 (retained as is in the overall average).

The task is to produce two Puppet modules that will install an SSH server and the Apache service on Ubuntu and CentOS.

These modules should be usable on both systems (include a conditional test that returns an error if the operating system is not compatible).
In the module directory, we will find the following 3 directories: manifest, files, and templates.

The manifests directory will contain the Puppet code, files will contain the configuration files, and templates will contain the configuration files with variables.   


The manifests directory will contain the following 5 files:
• init.pp (managing dependencies of other classes)
• params.pp (relevant parameters for installing the required packages)
• install.pp (installation of the required packages)
• config.pp (management of configuration files)
• service.pp (service management)


Regarding the SSH module, we will manage a template file "ssh/templates/sshd_config.erb" for the service configuration:
• Set the connection protocol to version 2;
• Connect on port 23 if on a physical machine, port 24 on a virtual machine.

Syntax checker : 
   • puppet-lint 
   • puppet parser validate

Example : 
```bash
puppet-lint modules/custom_*/manifests/*.pp       
puppet parser validate modules/custom_*/manifests/*.pp     
```

## Run Puppet

### with node.pp (run all the modules)
```bash
sudo puppet apply --modulepath=Puppet/modules/ Puppet/manifests/node.pp
```

### run a apache2 individually in apache environnement
```bash
git checkout puppet-apache
sudo puppet apply --modulepath=Puppet/modules/ Puppet/manifests/node.pp
```
### use only ssh module
```bash
sudo puppet apply --modulepath=/home/puppet -e "include custom_sshd"
```

### Note :

You will need to change the hostname in the `manifests/node.pp` in order to fit your hostname



## In a nutshell, Puppet :

Will install and configure a service on a host, using the puppet package. So, you need to have the puppet package already installed on your machine before running puppet. Additionally, ensure that you have the necessary permissions to install and manage packages, as well as to execute Puppet commands. Once Puppet is installed and configured, you can create and apply Puppet manifests and modules to automate the setup and management of various services on your host. This process typically involves defining the desired state of your system in Puppet code and using the Puppet agent to enforce this state, thereby ensuring consistent and repeatable configurations across your infrastructure.

# Comparison of the two tools

Ease of Learning and Use

Ansible is much simpler and more efficient for small to medium infrastructures. It is easier to handle, with simple and understandable commands.
Puppet, on the other hand, is very difficult to master. It requires extensive configuration to finally make an infrastructure work.
Configuration Management (Hiera vs Ansible Inventories)

Ansible has a major drawback in configuration management. In large infrastructures (more than 500 machines), we can quickly get lost with the number of playbooks, roles, etc.
Puppet is very effective in large infrastructures because it inherently requires a strict and clear hierarchy. This makes configuration management practical.
Flexibility and Modularity

During major updates, it is common to encounter problems with Ansible, and fixing them can take a lot of time (since its hierarchy is not practical for massive use). For Puppet, it is completely the opposite.
Specific Use Cases and Performance

We have seen that Puppet is much more performant in terms of performance compared to Ansible.
Ansible is recommended for relatively simple solutions and in small to medium-sized enterprises.
Puppet, however, is highly recommended for large enterprises (large infrastructures). Thanks to its clean and detailed hierarchy, it is easy to expand the infrastructure.